# class: profile::base
# Does baseline things

class profile::base {
include ntp

  if $facts['os']['family'] == 'RedHat' {
    package { ['bash-completion', 'tree', 'zsh']:
      ensure => installed,
    }
  }
  file { ['/etc/puppetlabs/facter','/etc/puppetlabs/facter/facts.d']:
    ensure => directory,
  }

  profile::user{'bob':
    ensure => present,
    uid    => 1847,
  }
}
