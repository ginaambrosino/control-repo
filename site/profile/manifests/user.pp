# define 
define profile::user (
  $uid,
  $ensure = 'present',
) {
  user {$title:
  ensure     => $ensure,
  uid        => $uid,
  shell      => '/bin/zsh',
  managehome => true,
  }
}
